<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head; ?>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>
<body <?php print theme("onload_attribute"); ?>>
     
<div id="page">

<div id="syndicate">
</div> <!-- syndicate -->

<div id="marquee"><img src="<?php print base_path() . $directory; ?>/i/marquee.jpg" alt="cherry blossoms" /></div> <!-- marquee -->

<div id="info"><?php print $info; ?></div>

<div id="header">
	<div id="headerimg">
		<h1><a href="<?php print check_url($base_path); ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></h1>

    <div class="description"><?php print $site_slogan; ?></div>
        </div>  <!-- headerimg -->
</div> <!-- header -->

<?php if ($mission): ?>

   <div id="mission"><?php print $mission; ?></div>

<?php endif; ?>

<hr />

<div id="content" class="narrowcolumn">

	<?php if ($messages != ""): ?>
		<?php print $messages ?>
	<?php endif; ?> 	
	<?php if ($tabs != ""): ?>
		<?php print $tabs ?>
	<?php endif; ?>
				
	<?php if ($help != ""): ?>
		<p id="help"><?php print $help ?></p>
	<?php endif; ?>

	<!-- start main content -->
<?php print $content; ?>
	<!-- end main content -->

</div> <!-- content -->

<div id="sidebar">

<?php if ($sidebar != ""): ?>

<?php print $sidebar ?>

<?php endif; ?>		

</div> <!-- sidebar -->

<div id="footer">

<p><?php print $feed_icons; ?></p>

<p>Site Design by</p>
<p><a title="Web 2.0 Community Development and Web Design" href="http://raincitystudios.com"><img src="<?php print base_path() . $directory; ?>/i/rcsmini.gif" alt="Raincity Studios" style="margin: 0 auto; display: block;" /></a></p>

<?php print $footer; ?>

</div>

</div>

 <?php print $closure;?>

  </body>
</html>
